all: coverletter master

coverletter: 
	mkdir -p build
	pdflatex --output-directory build src/coverLetter.tex
	cd ..
	cp build/*.pdf ./

master:
	mkdir -p build
	pdflatex --output-directory build src/masterResume.tex
	cd ..
	cp build/*.pdf ./

master-pub:
	mkdir -p build
	pdflatex --output-directory build src/masterResume.tex
	bibtex build/masterResume.aux
	pdflatex --output-directory build src/masterResume.tex
	pdflatex --output-directory build src/masterResume.tex
	cd ..
	cp build/*.pdf ./

clean:
	rm -rf build
	rm masterResume.pdf
